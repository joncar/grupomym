<?php
    class navbar{
        var $brandlabel;
        var $brandlink;
        var $collapse;
        function __construct()
        {
           $this->CI =& get_instance();
        }
        
        function additem($label = 'Item',$link = '#',$class = '')
        {
            if(is_string($label))
            array_push($this->collapse['nav'],array('label'=>$label,'link'=>$link,'class'=>$class));
           
            else if(is_array($label))
            $this->collapse['nav'] = $label;
        }
        
        function init($brand = 'Home', $brandlink = '#')
        {
           $this->brandlabel = $brand;
           $this->brandlink = $brandlink;
           $this->collapse['nav'] = array();
           $this->collapse['form'] = null;
        }
        
        function setform($id,$method,$action,$button)
        {
            $this->collapse['form']['id'] = $id;
            $this->collapse['form']['method'] = $method;
            $this->collapse['form']['action'] = $action;
            $this->collapse['form']['button'] = $button;
        }
        
        function additemform($item)
        {
           array_push($this->collapse['form']['items'],$item);
        }
        
        function draw()
        {
            $data = array('brandlabel'=>$this->brandlabel,'brandlink'=>$this->brandlink);
            $data['collapse'] = array('nav'=>$this->collapse['nav'],'form'=>$this->collapse['form']);
            return $this->CI->load->view('predesign/navbar',array('data'=>$data),TRUE);
        }
    }
    
    class bootstrap{
        function __construct()
        {
            $this->navbar = new navbar();
            $this->CI =& get_instance();
        }
    }
?>
