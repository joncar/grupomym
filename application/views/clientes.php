<? $this->load->view('includes/subheader'); ?>
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <? $active = 'class="active"'; ?>
  <? foreach($this->db->get('clientes')->result() as $c): ?>
  <li <?= $active ?>><a href="#<?= $c->id ?>" data-toggle="tab"><?= $c->leyenda ?></a></li>
  <? $active = '' ?>
  <? endforeach ?>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <? $active = 'active'; ?>
  <? foreach($this->db->get('clientes')->result() as $c): ?>
  <div class="tab-pane <?= $active ?> row" id="<?= $c->id ?>">
      <div class='col-xs-12'><b><?= !empty($c->nombre)?$c->nombre:'' ?></b></div>
      <div class='col-xs-12'><?= img('files/'.$c->imagen,'max-width:500px') ?></div>
  </div>
  <? $active = '' ?>
  <? endforeach ?>
</div>
