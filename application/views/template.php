<!Doctype html>
<html lang="es">
	<head>
		<title>Grupo M y M C.A.</title>
		<meta charset="utf8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
		<script src="<?= base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
		<script src="<?= base_url('assets/frame.js') ?>"></script>
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/style.css') ?>">
                <script src="<?= base_url('assets/jquery.colorbox-min.js') ?>"></script>
                 <link href="<?= base_url('assets/colorbox.css') ?>" rel="stylesheet" type="text/css">
		<?php 
		if(!empty($css_files) && !empty($js_files)):
		foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
		<?php endforeach; ?>
		<?php foreach($js_files as $file): ?>
		<script src="<?= $file ?>"></script>
		<?php endforeach; endif; ?>
	</head>
	<body>
            <section class="container">
            <? $this->load->view('includes/header'); ?>
            <? $this->load->view($view) ?>
            <? $this->load->view('includes/footer') ?>
            </section>
        </body>
</html>