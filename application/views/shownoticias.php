<? if($noticia->num_rows>0): ?>
<? $noticia = $noticia->row() ?>
<h1 class="divider"><?= $noticia->titulo ?></h1>
<div class="row">
    <div class="col-lg-2">
        <?= img('files/'.$noticia->imagen,'width:100%;') ?>
    </div>
    <div class="col-lg-10">
        <?= $noticia->texto ?>
        <div class="row">
            <div class="col-lg-8"><b>Fuente</b>: <?= $noticia->fuentes ?></div>
            <div class="col-lg-4"><b>Fecha</b>: <?= $this->querys->fecha($noticia->fecha) ?></div>
        </div>
    </div>
</div>
<? endif; ?>
