<? $this->load->view('includes/subheader'); ?>

<div class="panel-group" id="accordion">

<? foreach($this->db->get('proyectos')->result() as $p): ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><?= $p->nombre ?></a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body row">
          <? foreach($this->db->get_where('proyectos_fotos',array('id_proyecto'=>$p->id))->result() as $f): ?>
          <a class="group<?= $p->id ?> cboxElement col-xs-2" title="<?= $f->leyenda ?>" href="<?= base_url('files/'.$f->foto) ?>">
              <div class="col-xs-12" style="text-align:center; font-weight:bold;"><?= $f->leyenda ?></div>
              <div class="col-xs-12"><?= img('files/'.$f->foto,'width:100%',TRUE,'') ?></div>
          </a>
          <? endforeach ?>
      </div>
    </div>
  </div>
<? endforeach ?>
    <script>
        $(document).ready(function(){
            <? foreach($this->db->get('proyectos')->result() as $p): ?>
                $(".group<?= $p->id ?>").colorbox({rel:'group<?= $p->id ?>'});
            <? endforeach ?>
        });
    </script>
</div>
