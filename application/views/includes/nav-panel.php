<ul class="well nav nav-pills nav-stacked">
    <li style="text-align:center"><b>Menu</b></li>
    <li><a href="<?= base_url('panel') ?>"><i class="glyphicon glyphicon-wrench"></i> Panel de control</a></li>
    <?if($_SESSION['cuenta']==99): ?>
    <li><b>Administracion</b></li>
    <li class='divider'></li>
    <?if($this->querys->getAccess('usuarios')): ?><li><a href="<?= base_url('usuarios') ?>"><i class="glyphicon glyphicon-user"></i> Usuarios</a></li><? endif ?>
    <?if($this->querys->getAccess('paginas')): ?><li><a href="<?= base_url('paginas') ?>"><i class="glyphicon glyphicon-list-alt"></i> Paginas</a></li><? endif ?>
    <?if($this->querys->getAccess('link')): ?><li><a href="<?= base_url('link') ?>"><i class="glyphicon glyphicon-link"></i> Links de interes</a></li><? endif ?>
    <?if($this->querys->getAccess('leyes')): ?><li><a href="<?= base_url('leyes') ?>"><i class="glyphicon glyphicon-tower"></i> Leyes</a></li><? endif ?>
    <?if($this->querys->getAccess('boletines')): ?><li><a href="<?= base_url('boletines') ?>"><i class="glyphicon glyphicon-envelope"></i> Boletines</a></li><? endif ?>
    <?if($this->querys->getAccess('inmuebles')): ?><li><a href="<?= base_url('inmuebles') ?>"><i class="glyphicon glyphicon-bookmark"></i> Inmuebles</a></li><? endif ?>
    <?if($this->querys->getAccess('token')): ?><li><a href="<?= base_url('token') ?>"><i class="glyphicon glyphicon-lock"></i> Token</a></li><? endif ?>
    <?if($this->querys->getAccess('noticias')): ?><li><a href="<?= base_url('noticias') ?>"><i class="glyphicon glyphicon-calendar"></i> Noticias</a></li><? endif ?>
    <?if($this->querys->getAccess('ajustes')): ?><li><a href="<?= base_url('ajustes') ?>"><i class="glyphicon  glyphicon-wrench"></i> Ajustes</a></li><? endif ?>
    <?if($this->querys->getAccess('banner')): ?><li><a href="<?= base_url('banner') ?>"><i class="glyphicon  glyphicon-picture"></i> Banner</a></li><? endif ?>
    <?if($this->querys->getAccess('proyectos')): ?><li><a href="<?= base_url('proyectos') ?>"><i class="glyphicon  glyphicon-picture"></i> Proyectos</a></li><? endif ?>
    <?if($this->querys->getAccess('clientes')): ?><li><a href="<?= base_url('clientes') ?>"><i class="glyphicon  glyphicon-picture"></i> Clientes</a></li><? endif ?>
    <? endif ?>
    <li class="divider"></li>
    <li><a href="<?= base_url('main/unlog') ?>"><i class="glyphicon glyphicon-remove-circle"></i> Desconectar</a></li>
</ul>

