<? if($data->num_rows>0): ?>
<? foreach($data->result() as $n): ?>
<div class="row">
    <div class="col-lg-1"><?= img('files/'.$n->imagen,'width:100%;') ?></div>
    <div class="col-lg-11" align="justify">
    <p><a href="<?= site_url('mostrar-noticia/'.$n->id."_".str_replace("+","-",urlencode($n->titulo))) ?>"><?= $n->titulo ?></a></p>
    <?= $n->texto ?>
    </div>
</div>
<? endforeach ?>
<? endif ?>