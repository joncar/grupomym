<h1>Inmuebles</h1>
        <div>
        <? 
            $data = array(''=>'Seleccione');
            foreach($this->db->get('localidad')->result() as $c)
            $data[$c->id] = $c->nombre;
            $selected = empty($_GET['ciudad'])?'0':$_GET['ciudad'];
            echo "<div class='row'><div class='col-lg-4'>Ciudad: </div><div class='col-lg-8'>".form_dropdown('ciudad',$data,$selected,'id="fciudad"').'</div></div>';
        ?>
        </div>
        <div>
        <? 
            $data = array(''=>'Seleccione');
            foreach($this->db->get('operaciones')->result() as $c)
            $data[$c->id] = $c->nombre;
            $selected = empty($_GET['operacion'])?'0':$_GET['operacion'];
            echo "<div class='row'><div class='col-lg-4'>Operacion: </div><div class='col-lg-8'>".form_dropdown('operaciones',$data,$selected,'id="foperaciones"').'</div></div>';
        ?>
        </div>
        <div>
        <? 
            $data = array(''=>'Seleccione');
            foreach($this->db->get('tipos')->result() as $c)
            $data[$c->id] = $c->nombre;
            $selected = empty($_GET['tipo'])?'0':$_GET['tipo'];
            echo "<div class='row'><div class='col-lg-4'>Tipo: </div><div class='col-lg-8'>".form_dropdown('tipos',$data,$selected,'id="ftipos"').'</div></div>';
        ?>
        </div>
        <div>
        <? 
            $data = array(''=>'Seleccione','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');
            $selected = empty($_GET['hab'])?'0':$_GET['hab'];
            echo "<div class='row'><div class='col-lg-4'>Habitaciones: </div><div class='col-lg-8'>".form_dropdown('hab',$data,$selected,'id="fhab"').'</div></div>';
        ?>
        </div>
        <div>
        <? 
            $data = array(''=>'Seleccione','0'=>'0','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');
            $selected = empty($_GET['est'])?'0':$_GET['est'];
            echo "<div class='row'><div class='col-lg-4' title=\"Puestos de estacionamiento\">P. Est: </div><div class='col-lg-8'>".form_dropdown('est',$data,$selected,'id="fest"').'</div></div>';
        ?>
        </div>
        <div>
        <? 
            $data = array(''=>'Seleccione','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');
            $selected = empty($_GET['banos'])?'0':$_GET['banos'];
            echo "<div class='row'><div class='col-lg-4' title=\"Puestos de estacionamiento\">Baños: </div><div class='col-lg-8'>".form_dropdown('banos',$data,$selected,'id="fbanos"').'</div></div>';
        ?>
        </div>