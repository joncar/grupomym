<? $this->bootstrap->navbar->init('<i class="glyphicon glyphicon-home"></i> Home',site_url()) ?>
<? $this->bootstrap->navbar->additem('<i class="glyphicon glyphicon-user"></i> Quienes somos',site_url(date("Y").'/paginas/quienes-somos'))?>
<? $this->bootstrap->navbar->additem('<i class="glyphicon glyphicon-briefcase"></i> Servicios',site_url(date("Y").'/paginas/servicios'))?>
<? $this->bootstrap->navbar->additem('<i class="glyphicon glyphicon-tag"></i> Documentación','#','dropdown')?>
<? $this->bootstrap->navbar->additem('<i class="glyphicon glyphicon-bookmark"></i> Inmuebles',site_url(date("Y").'/in'))?>
<? $this->bootstrap->navbar->additem('<i class="glyphicon glyphicon-earphone"></i> Contactenos',site_url(date("Y").'/paginas/contactenos'))?>
<?= $this->bootstrap->navbar->draw() ?>
<center><small>Copyright © 2013 - 2014 | AFM Consultores, C.A.| Rif. J-29878605-1</small></center>