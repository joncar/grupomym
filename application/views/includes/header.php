<header>
    <div class="row">
        <div class="col-xs-4"><?= img('img/Logo.png') ?><br/>Rif: J-00126312-8</div>
    </div>
<? $this->bootstrap->navbar->init('<i class="glyphicon glyphicon-home"></i> Home',site_url()) ?>
<? $this->bootstrap->navbar->additem('<i class="glyphicon glyphicon-user"></i> Quienes somos',site_url(date("Y").'/paginas/quienes-somos'))?>
<? $this->bootstrap->navbar->additem('<i class="glyphicon glyphicon-briefcase"></i> Servicios',site_url(date("Y").'/paginas/servicios'))?>
<? $this->bootstrap->navbar->additem('
    <i class="glyphicon glyphicon-tag"></i> Documentación <b class="caret"></b>
    <ul class="dropdown-menu">
        <li><a href="'. site_url('v/link') .'">Links de interes</a></li>
        <li><a href="'.site_url('v/leyes') .'">Descarga de leyes</a></li>
        <li><a href="'. site_url('lista-de-noticias') .'">Noticias</a></li>
        <li><a href="'.site_url('v/proyectos').'">Proyectos</a></li>
        <li><a href="'.site_url('v/clientes').'">Clientes</a></li>
    </ul>','#','dropdown')?>
<? $this->bootstrap->navbar->additem('<i class="glyphicon glyphicon-bookmark"></i> Inmuebles',site_url(date("Y").'/in'))?>
<? $this->bootstrap->navbar->additem('<i class="glyphicon glyphicon-earphone"></i> Contactenos',site_url(date("Y").'/paginas/contactenos'))?>
<?= $this->bootstrap->navbar->draw() ?>
</header>