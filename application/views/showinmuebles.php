<? if($inmueble->num_rows>0): ?>
<? $inmueble = $inmueble->row() ?>
<h2><?= $inmueble->titulo ?></h2>
<div class="row">
    <div class="col-lg-4">
        <? $data = array() ?>
        <? if(!empty($inmueble->foto1)) array_push($data,$inmueble->foto1) ?>
        <? if(!empty($inmueble->foto2)) array_push($data,$inmueble->foto2) ?>
        <? if(!empty($inmueble->foto3)) array_push($data,$inmueble->foto3) ?>
        <? if(!empty($inmueble->foto4)) array_push($data,$inmueble->foto4) ?>
        <? if(!empty($inmueble->foto5)) array_push($data,$inmueble->foto5) ?>
        <? if(!empty($inmueble->foto6)) array_push($data,$inmueble->foto6) ?>
        <? $this->load->view('predesign/carousel',array('data'=>$data)); ?>
    </div>
    <div class="col-lg-8 well">
        <div class="row">
            <div class="col-lg-3">
                <b>Precio</b><br/>
                <?= $this->querys->price($inmueble->precio) ?>
            </div>
            <div class="col-lg-3">
                <b>Fecha de publicación</b><br/>
                <?= $this->querys->fecha($inmueble->fecha) ?>
            </div>
            <div class="col-lg-3">
                <b>Operación</b><br/>
                <?= $inmueble->operacionstr ?>
            </div>
            <div class="col-lg-3">
                <b>Tipo de inmueble</b><br/>
                <?= $inmueble->tipostr ?>
            </div>
        </div>
        <p><b>Dirección</b><br/>
            <?= $inmueble->estado ?><br/>
            <?= $inmueble->direccion ?>
        </p>
        <p><b>Detalles</b><br/>
            <?= $inmueble->descripcion ?>
        </p>
    </div>
</div>
<? endif ?>