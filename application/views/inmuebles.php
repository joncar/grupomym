<?php
//if(empty($_SESSION['token']))
//{
//    header("Location:".base_url('in'));
//}
?>
<? $this->load->view('includes/subheader'); ?>
<section class="row">
    <article class="col-lg-8">
        <div class="row dividir" style="margin:10px; padding:10px">
           <div class="col-lg-12 row">
                <form action="" method="get" id="formsearch">
                    <div class="col-xs-8">
                <?= form_input('search',!empty($_GET['search'])?$_GET['search']:'','class="form-control" placeholder="Buscar"') ?>
                <input type="hidden" name="ciudad" id="ciudad">
                <input type="hidden" name="operacion" id="operacion">
                <input type="hidden" name="tipo" id="tipo">
                <input type="hidden" name="minprecio" id="minprecio">
                <input type="hidden" name="maxprecio" id="maxprecio">
                <input type="hidden" name="hab" id="hab">
                <input type="hidden" name="est" id="est">
                <input type="hidden" name="banos" id="banos">
                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-success">Buscar</button>
                    </div>
                </form>
           </div>
        </div>
        <div>
        <? $this->load->view('includes/listinmuebles',array('data'=>$this->querys->getInmuebles())) ?>
        </div>
    </article>
    <div class="col-lg-4" style="padding:10px;">
        <? $this->load->view('includes/filtros'); ?>
        <div style="margin-bottom:15px;">
            <b>Por precio</b>
            <div class="row">
                <form action="">
                <? $selected = empty($_GET['minprecio'])?'':$_GET['minprecio']; ?>   
                <div class="col-sm-6"><?= form_input('text',$selected,'placeholder="Min" id="fminprecio" class="form-control"') ?></div>
                <? $selected = empty($_GET['maxprecio'])?'':$_GET['maxprecio']; ?> 
                <div class="col-sm-6"><?= form_input('text',$selected,'placeholder="Max" id="fmaxprecio" class="form-control"') ?></div>
                </form>
            </div>
        </div>
        <h1>Enlaces de interés</h1>
        <div class="well">
            <? foreach($this->db->get('links')->result() as $r): ?>
            <li><a href="<?= $r->url ?>"><?= $r->titulo ?></a></li>
            <? endforeach ?>
        </div>
    </div>
</section>
<script>
        function sub()
        {
            $("#ciudad").val($("#fciudad").val());
            $("#operacion").val($("#foperaciones").val());
            $("#tipo").val($("#ftipos").val());
            $("#minprecio").val($("#fminprecio").val());
            $("#maxprecio").val($("#fmaxprecio").val());
            $("#formsearch").submit();
        }
        $("#fciudad,#foperaciones,#ftipos,#fminprecio,#fmaxprecio").change(function(){sub();});
</script>