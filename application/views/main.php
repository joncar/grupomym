<? $this->load->view('includes/subheader'); ?>
<article>
                    <div class="row">
                    <div class="col-lg-8">
                        <div class="dividir col-lg-12 "><h4>GRUPO M&AMP;M C.A</h4></div>
                        <div class="col-lg-12">
                        <?= substr(strip_tags($this->db->get_where('paginas',array('url'=>'quienes-somos'))->row()->texto)."...",0,400)." <a href='".site_url(date("Y").'/paginas/quienes-somos')."'>Leer más</a>" ?>
                        </div>
                        <div class="dividir col-lg-12 "><h4>Noticias</h4></div>
                        <div class="col-lg-12">
                        <? $this->db->limit('3'); $noticias = $this->querys->getNoticias(); ?>
                        <? if($noticias->num_rows>0) $noticias->row()->texto.= '<p align="right"><a href="'.site_url('lista-de-noticias').'">Leer todas</a></p>'; ?>
                        <? $this->load->view('includes/noticias',array('data'=>$noticias)) ?>
                        </div>
                        
                    </div>
                    <div class="col-lg-4">
                        <form action="<?= site_url('2014/in') ?>" method="get" id="formsearch">
                            <? $this->load->view('includes/filtros'); ?>
                            <p align="center" style="margin-top:10px"><button class="btn btn-success" type="submit">Consultar</button></p>
                        </form>
                        <h1>Enlaces de interés</h1>
                        <div class="well">
                            <? foreach($this->db->get('links')->result() as $r): ?>
                            <li><a href="<?= $r->url ?>"><?= $r->titulo ?></a></li>
                            <? endforeach ?>
                        </div>
                        <h1>Boletin Diario</h1>
                        <div class="well">
                            <form role="form" method='post' onsubmit='return validar(this)' action='<?= base_url('main/regbol') ?>' class="form-horizontal"  >
                                <? if(!empty($msj2))echo $msj2 ?>
                                <h5>Subscribete a nuestro boletin diario</h5>
                              <?= input('email','Email','email') ?>
                              <div align="center"><button type="submit" class="btn btn-success">Subscribir</button></div>
                            </form>
                        </div>
                        <h1>Nuestras redes sociales</h1>
                        <div class="well">
                            <div class="row ">
                                <?=  $this->querys->get_social('twitter'); ?>
                                <?=  $this->querys->get_social('facebook'); ?>
                                <?=  $this->querys->get_social('youtube'); ?>
                                <?=  $this->querys->get_social('google'); ?>
                            </div>
                        </div>
                    </div>
                    </div>
                </article>