<?php

class Querys extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_links()
    {
        return $this->db->get('links');
    }
    
    public function price($val)
    {
	return number_format($val,2,',','.').' '.$this->db->get('ajustes')->row()->moneda;
    }
    
     public function fecha($val)
    {
	return date($this->db->get('ajustes')->row()->formato_fecha,strtotime($val));
    }
    
    public function getAccess($controller)
    {
        $privilegios = $_SESSION['privilegios'];
        $privilegios = explode(",",$privilegios);
        if(in_array($controller, $privilegios) || in_array('todos',$privilegios))
        return true;
        return false;
    }
    
     public function get_leyes()
    {
        return $this->db->get('leyes');
    }
    
    public function get_social($social)
    {
        switch($social){
            case 'facebook': return !empty($this->db->get('ajustes')->row()->facebook)?'<div class="col-lg-2"><a href="'.$this->db->get('ajustes')->row()->facebook.'">'.img('img/facebook.png','width:70%').'</a></div>':''; break;
            case 'twitter': return !empty($this->db->get('ajustes')->row()->twitter)?'<div class="col-lg-2"><a href="'.$this->db->get('ajustes')->row()->twitter.'">'.img('img/twitter.png','width:70%').'</a></div>':''; break;
            case 'google': return !empty($this->db->get('ajustes')->row()->google)?'<div class="col-lg-2"><a href="'.$this->db->get('ajustes')->row()->google.'">'.img('img/google.png','width:70%').'</a></div>':''; break;
            case 'youtube': return !empty($this->db->get('ajustes')->row()->youtube)?'<div class="col-lg-2"><a href="'.$this->db->get('ajustes')->row()->youtube.'">'.img('img/youtube.jpg','width:70%').'</a></div>':''; break;
        }
    }
    
    function getInmueble($id)
    {
        $this->db->select('inmuebles.*,localidad.nombre as estado,operaciones.nombre as operacionstr,tipos.nombre as tipostr');
        $this->db->join('localidad','localidad.id=inmuebles.ciudad');
        $this->db->join('operaciones','operaciones.id=inmuebles.operacion');
        $this->db->join('tipos','tipos.id=inmuebles.tipo');
        return $this->db->get_where('inmuebles',array('inmuebles.id'=>$id));
    }
    
    public function getInmuebles()
    {
        if(!empty($_GET))
        {
            if(!empty($_GET['ciudad']))$this->db->where('ciudad',$_GET['ciudad']);
            if(!empty($_GET['operacion']))$this->db->where('operacion',$_GET['operacion']);
            if(!empty($_GET['tipo']))$this->db->where('tipo',$_GET['tipo']);
            if(!empty($_GET['minprecio']))$this->db->where('precio >=',$_GET['minprecio']);
            if(!empty($_GET['maxprecio']))$this->db->where('precio <=',$_GET['maxprecio']);
            if(!empty($_GET['search']))$this->db->like('titulo',$_GET['search']);
            if(!empty($_GET['hab']))$this->db->where('habitaciones',$_GET['hab']);
            if(!empty($_GET['est']))$this->db->where('estacionamiento',$_GET['est']);
            if(!empty($_GET['banos']))$this->db->where('banos',$_GET['banos']);
        }
        $this->db->select('inmuebles.*,localidad.nombre as estado');
        $this->db->join('localidad','localidad.id=inmuebles.ciudad');
        return $this->db->get('inmuebles');
    }
    
    function getNoticias($id = '')
    {
        if(!empty($id) && is_numeric($id) && $id>0)$this->db->where('id',$id);
        $this->db->order_by('fecha','DESC');
        return $this->db->get('noticias');
    }
}
?>
