<?php
require_once('panel.php');
class Clientes extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('clientes'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('clientes');
            $crud->set_subject('Clientes');
            //Fields
            
            //unsets
            
            //Displays
            
            //Fields types
            $crud->set_field_upload('imagen','files');
            //Validations
            $crud->required_fields('imagen','leyenda');
            
            //Callbacks

            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'usuarios';
            $this->loadView($output);
        }   
}

?>