<?php
require_once('panel.php');
class Boletines extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('boletines'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('boletines');
            $crud->set_subject('Boletines');
            //Fields
            $crud->edit_fields('titulo','texto','destinatario');
            //unsets
            
            //Displays
            $crud->display_as('texto','Contenido que se va a enviar');
            $crud->columns('titulo','destinatario','send');
            //Fields types
            
            $emails = $this->db->get('user');
            $data['Todos'] = 'Todos';
            $data['Usuarios'] = 'User';
            $data['Admin'] = 'Admin';
            foreach($emails->result() as $e)
            $data[$e->email] = $e->email;
            $crud->field_type('destinatario','set',$data);
            $crud->set_field_upload('foto','files');
            //Validations
            $crud->required_fields('titulo','destinatario');
            //Callbacks
            $crud->callback_column('send',array($this,'sendColumn'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'usuarios';
            $this->loadView($output);
        }
        function sendColumn($val,$row)
	{
		return '<a target="_new" href="'.base_url('cron/SendBoletin/'.$row->id).'">Enviar boletin</a>';	
	}
}

?>