<?php
require_once('main.php');
class Cron extends Main{
    function __consctruct()
    {
        parent::__construct();
    }
    
    function SendBoletin($id = '')
    {
        if(!empty($id)){
        foreach($this->db->get('boletines')->result() as $b)
        {
            //Tomar destinatarios
            if($b->destinatario=='Todos')
            {
                $emails = '';
                foreach($this->db->get('boletin_email')->result() as $e)
                $emails.= $e->email.",";
            }
            else $emails = $b->destinatario;
            //Tomar contenido
	    $notice = '';
	    $no = $this->db->get_where('boletines',array('id'=>$id))->row();
            $notice.= '<div style="background:lightgray; margin:10px; width:100%; display:inline-block; padding:10px;"><div style="padding:20px;"><h2>'.$no->titulo.'</h2>';
	    $notice.= '<div><div style="float:left; margin-right:10px;">'.img('files/'.$no->foto,'width:100px').'</div><div style="float:left">'.$no->texto.'</div></div></div></div>';
            //Enviar email
            foreach(explode(",",$emails) as $e)
            {
                correo($e,$b->titulo,$notice);
            }
	    echo 'Boletin enviado';
        }
        }
    }
}