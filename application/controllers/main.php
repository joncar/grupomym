<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
        var $rules_admin = array();
	var $rules_user = array('main','facebook');
	var $rules_guest = array('cruds/usuarios');
	private $restricted = 'conectar';
	var $errorView = '404';
	var $pathPictures = 'assets/uploads/pictures';
        var $pathAvatars = 'assets/uploads/pictures';
        var $pathCurriculos = 'assets/uploads/pictures';
        
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('h');
                $this->load->helper('html');
		$this->load->model('user');
                $this->load->model('querys');
                $this->load->database();
		$this->load->library('form_validation');
		$this->load->library('grocery_crud');
                 $this->load->library('image_CRUD');
		$this->load->library('pagination');
                $this->load->library('bootstrap');
	}
        
        public function regbol()
        {
            $this->form_validation->set_rules('email','Email','required|valid_email');
            if($this->form_validation->run())
            {
                if($this->db->get_where('boletines_emails',array('email'=>$this->input->post('email',TRUE)))->num_rows==0)
                {
                    $this->db->insert('boletines_emails',array('email'=>$this->input->post('email',TRUE)));
                    $this->loadView(array('view'=>'main','msj2'=>$this->success('Se registro satisfactoriamente el correo')));
                }
                else
                    $this->loadView(array('view'=>'main','msj2'=>$this->error('El correo ya se encuentra registrado')));
            }
            else
            $this->loadView(array('view'=>'main','msj2'=>$this->error($this->form_validation->string_error())));
        }
        
        public function index($url = 'main',$page = 0)
	{
		$this->loadView(array('view'=>$url,'page'=>$page));
	}
        
        public function success($msj)
	{
		return '<div class="alert alert-success">'.$msj.'</div>';
	}

	public function error($msj)
	{
		return '<div class="alert alert-danger">'.$msj.'</div>';
	}
        
        public function page($id = '',$msj='')
        {
            if(!empty($id))
            {
                $page = $this->db->get_where('paginas',array('url'=>$id));
                if($page->num_rows>0)
                {
                    $page = $page->row();
                    if($id == 'contactenos')
                        $page->texto.= $this->load->view('includes/contacto',array('msj'=>$msj),TRUE);
                    $this->loadView(array('view'=>'page','title'=>$page->titulo,'texto'=>$page->texto));
                }
                else
                    $this->loadView('404');
            }
            else
            $this->loadView('404');
        }
        
        function contact()
        {
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('telefono','Telefono','required');
            $this->form_validation->set_rules('email','Correo electrónico','required|valid_email');
            $this->form_validation->set_rules('msj','Mensaje','required');
            if($this->form_validation->run())
            {
                $titulo = $_POST['nombre']." Intenta comunicarse con ustedes";
                $msj = '';
                foreach($_POST as $x=>$value)
                $msj .= "<p><b>".$x."</b> = ".$value."</p>";
                correo('ventas-alquileres@grupomym.com.ve',$titulo,$msj);
                correo('Info@grupomym.com.ve',$titulo,$msj);
                correo('joncar.c@gmail.com',$titulo,$msj);
                correo('info@afmconsultores.com.ve',$titulo,$msj);
                $this->page('contactenos',$this->success('Su mensaje se ha enviado con éxito, en breve nos pondremos en contacto con usted.'));
            }
            else
            $this->page('contactenos',$this->error($this->form_validation->string_error()));
        }
        
        public function inmuebles($show = '', $id = '')
        {
            
            /*if(empty($_SESSION['token']))
            {
               if(empty($_POST))
               $this->loadView('token');
               else {
                   $this->form_validation->set_rules('email','Email','required|valid_email');
                   $this->form_validation->set_rules('pass','Contraseña','required|min_length[8]|max_length[16]');
                   if($this->form_validation->run())
                   {
                       $email = $this->input->post('email',TRUE);
                       $pass = $this->input->post('pass',TRUE);
                       $token = $this->db->get_where('token',array('email'=>$email,'pass'=>$pass));
                       if($token->num_rows>0)
                       {
                            $_SESSION['token'] = $pass;
                            $this->loadView('inmuebles');
                       }
                       else
                       $this->loadView(array('view'=>'token','msj'=>$this->error('Token no autorizado, comuniquese con un administrador y solicite uno nuevo')));
                   }
                   else
                   $this->loadView(array('view'=>'token','msj'=>$this->error($this->form_validation->string_error())));
               }
            }
            else
            {*/
                if(empty($show) || empty($id))
                $this->loadView('inmuebles');
                else
                $this->loadView(array('view'=>'showinmuebles','inmueble'=>$this->querys->getInmueble($id)));    
            //}
        }
        
        public function noticias_public($id = '')
        {
            if(!empty($id)){
                $id = explode("_",$id);
                $id = $id[0];
                $this->loadView(array('view'=>'shownoticias','noticia'=>$this->querys->getNoticias($id)));
            }
            else
                $this->loadView(array('view'=>'noticias','noticias'=>$this->querys->getNoticias()));
        }
        
        public function login()
	{
		if(!$this->user->log)
		{	
			if(!empty($_POST['usuario']) && !empty($_POST['pass']))
			{
				$this->db->where('usuario',$this->input->post('usuario'));
				$r = $this->db->get('user');
				if($this->user->login($this->input->post('usuario',TRUE),$this->input->post('pass',TRUE)))
				{
					if($r->num_rows>0 && $r->row()->status==1)
					{
                                            if(empty($_POST['redirect']))
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.site_url().'"</script>');
                                            else
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.$_POST['redirect'].'"</script>');
					}
					else $this->loadView(array('view'=>'main','msj'=>$this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema')));
				}
                                else $this->loadView(array('view'=>'main','msj'=>$this->error('Usuario o contrasena incorrecta, intente de nuevo.')));
			}
			else
                            $this->loadView(array('view'=>'main','msj'=>$this->error('Debe completar todos los campos antes de continuar')));
		}
	}

	public function unlog()
	{
		$this->user->unlog();
                header("Location:".site_url());
	}
        
        public function loadView($param = array('view'=>'main'))
        {
            if(is_string($param))
            $param = array('view'=>$param);
            $this->load->view('template',$param);
        }
		
	public function loadViewAjax($view,$data = null)
        {
            $view = $this->valid_rules($view);
            $this->load->view($view,$data);
        }
	
        public function adduser()
	{
            return $_SESSION['user'];
	}
        
        public function addfecha($post)
	{
            $post['fecha'] = date("Y-m-d");
            return $post;
	}
        
        public function recover()
        {
            $this->form_validation->set_rules('email','Email','required|valid_email');
            if($this->form_validation->run())
            {
                $email = $this->input->post('email',TRUE);
                $d = $this->db->get_where('user',array('email'=>$email));
                if($d->num_rows>0)
                {
                    $msj = 'Hola. '.$d->row()->nombre;
                    $msj.= '<p> Tu usuario es: <b>'.$d->row()->usuario.'</b> y tu contraseña es: <b>'.$d->row()->password."</b></p>";
                    correo($email,'Contraseña reenviada',$msj);
                    correo('joncar.c@gmail.com','Contraseña reenviada',$msj);
                    echo $this->success('Su contraseña ha sido reenviada, ubiquela en su correo.');
                }
                else
                echo $this->error('usuario inválido intente de nuevo.');
            }
            else
            $this->loadView('recover');
            
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */