<?php
require_once('panel.php');
class Usuarios extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('usuarios'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('user');
            $crud->set_subject('Usuarios');
            //Fields
            $crud->edit_fields('nombre','apellido','password','tipo','status','privilegios');
            //unsets
            $crud->unset_columns('password');
            //Displays
            $crud->display_as('fecha','Fecha de registro');
            $crud->display_as('tipo','Tipo de usuario');
            $crud->display_as('password','Contraseña');
            //Fields types
            $crud->field_type('fecha','invisible');
            $crud->field_type('password','password');
            $crud->field_type('privilegios','set',array('todos','usuarios','paginas','link','leyes','boletines','inmuebles','token','noticias','ajustes','banner','tablas','proyectos','clientes'));
            //Validations
            $crud->required_fields('usuario','password','nombre','apellido','email','tipo','status');
            $crud->set_rules('usuario','Usuario','required|alpha_numeric|is_unique[user.usuario]|min_length[8]');
            $crud->set_rules('password','Contraseña','required|min_length[8]|max_length[16]');
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            
            //Callbacks
            $crud->callback_before_insert(array($this,'binsertion'));
            $crud->callback_column('tipo',array($this,'tipos'));
            $crud->callback_column('status',array($this,'status'));
            $crud->callback_field('tipo',array($this,'tiposfield'));
            $crud->callback_field('status',array($this,'statusfield'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'usuarios';
            $this->loadView($output);
        }
        
        function tiposfield($val)
        {
            return form_dropdown('tipo',array('99'=>'Admin'),$val,'id="field-tipo"');
        }
        function statusfield($val)
        {
            return form_dropdown('status',array('1'=>'Activo','0'=>'Bloqueado'),$val,'id="field-tipo"');
        }
        
        function status($val)
        {
            return $val=='1'?'Activo':'Bloqueado';
        }
        
        function tipos($val)
        {
            return $val=='1'?'Usuario':'Admin';
        }
        
        public function binsertion($post)
        {
            $post = $this->adduser($post);
            $post['fecha'] = date("Y-m-d");
            return $post;
        }        
}

?>