<?php
require_once('panel.php');
class Paginas extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('paginas'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('paginas');
            $crud->set_subject('Paginas');
            //Fields
            $crud->edit_fields('titulo','texto','incrustar','incrustar2','incrustar3','incrustar4','incrustar5','incrustar6','incrustar7','incrustar8','incrustar9','incrustar10');
            //unsets
            $crud->unset_add();
            $crud->unset_delete();
            $crud->unset_columns('url','incrustar','incrustar2','incrustar3','incrustar4');
            //Displays
            $crud->display_as('texto','Contenido');
            
            //Fields types
            $crud->set_field_upload('incrustar','files');
            $crud->set_field_upload('incrustar2','files');
            $crud->set_field_upload('incrustar3','files');
            $crud->set_field_upload('incrustar4','files');
            $crud->set_field_upload('incrustar5','files');
            $crud->set_field_upload('incrustar6','files');
            $crud->set_field_upload('incrustar7','files');
            $crud->set_field_upload('incrustar8','files');
            $crud->set_field_upload('incrustar9','files');
            $crud->set_field_upload('incrustar10','files');
            //Validations
            $crud->required_fields('titulo','texto');
            
            //Callbacks
            $crud->callback_before_upload(array($this,'bupload'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'paginas';
            $this->loadView($output);
        }
        
        public function bupload($files_to_upload, $field_info)
        {
            $type = $files_to_upload[$field_info->encrypted_field_name]['type'];
            $image_info = getimagesize($files_to_upload[$field_info->encrypted_field_name]["tmp_name"]);
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            
            if ($type != 'image/png' && $type!='image/jpg' && $type!='image/jpeg')
            {
                return 'Extension no permitida';
            }
            return true;
        }
}

?>