<?php
require_once('panel.php');
class Inmuebles extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('inmuebles'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('inmuebles');
            $crud->set_subject('Inmuebles');
            //Relations
            $crud->set_relation('tipo','tipos','nombre');
            $crud->set_relation('operacion','operaciones','nombre');
            $crud->set_relation('ciudad','localidad','{nombre} {tabla}');
            //Fields
            
            
            //unsets
            
            //Displays
            $crud->display_as('ciudad','Estado');
            $crud->display_as('banos','Cantidad de baños');
            $crud->display_as('habitaciones','Cantidad de habitaciones');
            $crud->display_as('estacionamiento','Cantidad de puestos de estacionamiento');
            //Fields types
            $crud->field_type('fecha','invisible');
            for($i=1;$i<7;$i++)
            $crud->set_field_upload ('foto'.$i,'files');
            $crud->field_type('descripcion','textarea');
            //Validations
            $crud->required_fields('precio','titulo','descripcion','operacion','tipo','ciudad','direccion');
            $crud->set_rules('precio','Precio','required|numeric');
            $crud->set_rules('operacion','Operacion','required|integer');
            $crud->set_rules('tipo','Tipo','required|integer');
            $crud->set_rules('banos','Baños','required|integer');
            $crud->set_rules('habitaciones','Habitaciones','required|integer');
            $crud->set_rules('estacionamiento','Puestos de estacionamiento','required|integer');
            //Callbacks
            $crud->callback_before_insert(array($this,'binsertion'));
            $crud->callback_before_upload(array($this,'bupload'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'inmuebles';
            $this->loadView($output);
        }
        
        function binsertion($post)
        {
            $post['fecha'] = date("Y-m-d");
            return $post;
        }
        
        public function bupload($files_to_upload, $field_info)
        {
            $type = $files_to_upload[$field_info->encrypted_field_name]['type'];
            $image_info = getimagesize($files_to_upload[$field_info->encrypted_field_name]["tmp_name"]);
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            
            if ($type != 'image/png' && $type!='image/jpg' && $type!='image/jpeg')
            {
                return 'Extension no permitida';
            }
            else
            {
                if($image_width!=480 || $image_height!=360)
                return 'Dimensiones de imagen no soportadas, La imagen debe ser de 480x360';
                else
                return true;
            }
            
        }
}

?>