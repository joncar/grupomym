<?php
require_once('panel.php');
class Proyectos extends Panel{
 	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']) && !$this->querys->getAccess('proyectos'))
                header("Location:".base_url('panel'));
	}
        public function index()
        {
            $crud = new grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('proyectos');
            $crud->set_subject('Proyecto');
            //Fields
            $crud->columns('nombre','fotos');
            //unsets
            
            //Displays
            
            //Fields types
            
            //Validations
            $crud->required_fields('nombre');
            
            //Callbacks
            $crud->callback_column('fotos',array($this,'addfotos'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'usuarios';
            $this->loadView($output);
        }   
        function addfotos($val,$row)
        {
            return '<a href="'.base_url('proyectos/administrarFotos/'.$row->id).'">Añadir Fotos</a>';
        }
        
        function administrarFotos()
        {
            $image_crud = new image_CRUD();
            $image_crud->set_primary_key_field('id');
            $image_crud->set_url_field('foto');
            $image_crud->set_table('proyectos_fotos')
		->set_relation_field('id_proyecto')
		->set_ordering_field('prioridad')
		->set_image_path('files')
                ->set_title_field('leyenda');
			
		$output = $image_crud->render();
                $output->view = 'panel';
                $output->crud = 'fotos';
                $this->loadView($output);
        }
        
}

?>